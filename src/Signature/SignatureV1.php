<?php

namespace BoomApp\SDK\Signature;

use BoomApp\SDK\Utils\DateFormat;
use GuzzleHttp\Psr7\Request;
use Guzzle\Http\QueryString;

class SignatureV1
{

    const DEFAULT_PAYLOAD = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';

    protected $serviceName;

    protected $regionName;

    protected $extraSignableHeaders = array();

    protected $maxCacheSize = 50;

    protected $hashCache = array();

    protected $cacheSize = 0;

    protected $overrideDateHeaders = true;

    protected $config = array();

    protected $defaultConfig = array(
        'version'        =>    'BA1',
        'hashMethod'    =>    'HMAC',
        'hashAlgo'        =>  'SHA256',
        'headersPrefix' =>    'x-boom-'
    );

    protected static $validHashAlgos = array('SHA1','SHA256','SHA384','SHA512');

    protected $_lastSigningContext = array();

    public function __construct($serviceName = null, $regionName = null)
    {
        $this->setServiceName($serviceName);
        $this->setRegionName($regionName);

        //default configuration
        $this->config = $this->defaultConfig;
    }


    public function setConfig($signatureVersion = null, $signHashMethod = null, $signHashAlgo = null, $headersPrefix = null)
    {
        if (null !== $signatureVersion) {
            $this->config['version']    = strtoupper($signatureVersion);
        }
        if (null !== $signHashMethod) {
            $this->config['hashMethod'] = strtoupper($signHashMethod);
        }
        $this->setHeaderNamePrefix($headersPrefix);
        $this->setHashAlgo($signHashAlgo);
    }

    public function signRequest(Request $request, Credentials $credentials)
    {
        $headers = $request->getHeaders();
        // Requires a x-boom-date header or Date
        if ($this->isOverrideDateHeaders() || !$this->hasDateHeader($request)) {
            $timestamp = $this->getTimestamp();
            $longDate = gmdate(DateFormat::ISO8601, $timestamp);

            if ($request->hasHeader($this->getHeaderName('date')) || !$request->hasHeader('Date')) {
                //overides x-boom-date with current date (ISO8601 format)

                $headers = array_merge($headers, [ $this->getHeaderName('date') => [ $longDate ] ]);
            } else {
                //overides Date with current date (RFC1123 format)
                $headers = array_merge($headers, [ 'Date' => [ gmdate(DateFormat::RFC1123, $timestamp) ] ]);
            }
        } else {
            $longDate = $this->getDateHeader($request);
        }

        $shortDate = substr($longDate, 0, 8);

        // Parse the service and region or use one that is explicitly set
        $region = $this->regionName;
        $service = $this->serviceName;
        $reqTag = $this->getRequestTag();        //ba1_request
        $sigTag = $this->getSignatureTag();        //BA1-HMAC-SHA256

        $credentialScope = $this->createScope($shortDate, $region, $service, $reqTag);

        $payloadHash = $this->getPayloadHash($request);
        // var_dump("Payload hash:\n" . $payloadHash . "\n");

        $signingContext = $this->createSigningContext($request, $payloadHash);

        $signingContext['string_to_sign'] = $this->createStringToSign(
            $longDate,
            $credentialScope,
            $signingContext['canonical_request'],
            $sigTag
        );

        // var_dump("Canonical request:\n" . $signingContext['canonical_request'] . "\n");
        // var_dump("String to sign:\n" . $signingContext['string_to_sign'] . "\n");

        // Calculate the signing key using a series of derived keys
        $signingKey = $this->getSigningKey($shortDate, $region, $service, $credentials->getSecretKey(), $reqTag);
        $signature = hash_hmac($this->SHA(), $signingContext['string_to_sign'], $signingKey);

        $headerSignatureString = $sigTag. " "
            . "Credential={$credentials->getAccessKeyId()}/{$credentialScope}, "
            . "SignedHeaders={$signingContext['signed_headers']}, Signature={$signature}";

        $headers = array_merge($headers, [ 'Authorization' => $headerSignatureString ]);

        // Add debug information to the _lastSigningContext
        $this->_lastSigningContext = $signingContext;

        return $headers;
    }

    protected function createScope($shortDate, $region, $service, $requestTag)
    {
        return $shortDate
        . '/' . $region
        . '/' . $service
        . '/' . $requestTag;
    }

    public function getPayloadHash(Request $request)
    {
        // Calculate the request signature payload
        if ($request->hasHeader($this->getHeaderName('content-sha256'))) {
            // Handle streaming operations (e.g. Glacier.UploadArchive)
            return (string) $request->getHeader($this->getHeaderName('content-sha256'))[0];
        }

        $body = $request->getBody();
        // var_dump("Payload:\n" . $body . "\n");
        if (null !== $body && $body != '') {
            return hash($this->SHA(), $body);
        }

        return self::DEFAULT_PAYLOAD;
    }

    public function createSigningContext(Request $request, $payloadHash)
    {
        $signable = array(
            'host'        => true,
            'date'        => true,
            'content-md5' => true
        );

        //Add any extra headers to signable headers
        foreach ($this->extraSignableHeaders as $extraHeaderName) {
            $extraHeaderName = strtolower($extraHeaderName);
            $signable[ $extraHeaderName ] = true;
        }

        $canonHeaders = array();

        $headers = $request->getHeaders();
        
        foreach ($headers as $key => $values) {
            $key = strtolower($key);
            if (isset($signable[$key]) || $this->startsWithHeaderNamePrefix($key)) {
                if (count($values) == 1) {
                    $values = $values[0];
                } else {
                    sort($values);
                    $values = implode(',', $values);
                }

                $canonHeaders[$key] = $key . ':' . preg_replace('/\s+/', ' ', $values);
            }
        }

        ksort($canonHeaders);
        $signedHeadersString = implode(';', array_keys($canonHeaders));

        // Normalize the path as required by SigV4 and ensure it's absolute
        $canon = $request->getMethod() . "\n"
            . $this->createCanonicalizedPath($request) . "\n"
            . $this->getCanonicalizedQueryString($request) . "\n"
            . implode("\n", $canonHeaders) . "\n\n"
            . $signedHeadersString . "\n"
            . $payloadHash;

        return array(
            'canonical_request' => $canon,
            'signed_headers'    => $signedHeadersString
        );
    }

    public function createStringToSign($longDate, $credentialScope, $canonicalRequestString, $signatureTag)
    {
        return "{$signatureTag}\n{$longDate}\n{$credentialScope}\n" . hash($this->SHA(), $canonicalRequestString);
    }


    public function getSigningKey($shortDate, $region, $service, $secretKey, $requestTag = 'BA1_request')
    {
        $cacheKey = $shortDate . '_' . $region . '_' . $service . '_' . md5($secretKey);

        // Retrieve the hash form the cache or create it and add it to the cache
        if (!isset($this->hashCache[$cacheKey])) {
            // When the cache size reaches the max, then just clear the cache
            if (++$this->cacheSize > $this->maxCacheSize) {
                $this->hashCache = array();
                $this->cacheSize = 0;
            }
            $dateKey = hash_hmac($this->SHA(), $shortDate, $this->getSecretKeyPrefix() . $secretKey, true);
            $regionKey = hash_hmac($this->SHA(), $region, $dateKey, true);
            $serviceKey = hash_hmac($this->SHA(), $service, $regionKey, true);
            $this->hashCache[$cacheKey] = hash_hmac($this->SHA(), $requestTag, $serviceKey, true);
        }

        return $this->hashCache[$cacheKey];
    }

    public function createCanonicalizedPath(Request $request)
    {
        $doubleEncoded = rawurlencode(ltrim($request->getUri()->getPath(), '/'));

        return '/' . str_replace('%2F', '/', $doubleEncoded);
    }

    public function getCanonicalizedQueryString(Request $request)
    {
        $queryParams = $request->getUri()->getQuery();
        // var_dump("QueryParams:\n" . print_r($params, true) . "\n");

        if (empty($queryParams)) {
            return '';
        }

        return $queryParams;
    }

    public function setServiceName($service)
    {
        $this->serviceName = $service;
        return $this;
    }

    public function setRegionName($region)
    {
        $this->regionName = $region;
        return $this;
    }

    public function hasDateHeader(Request $request)
    {
        return ($request->hasHeader($this->getHeaderName('date')) || $request->hasHeader('Date'));
    }

    public function getDateHeader(Request $request)
    {
        $defaultError = "00000000T000000Z";

        if ($request->hasHeader($this->getHeaderName('date')) || !$request->hasHeader('Date')) {
            $longDate = $request->getHeader($this->getHeaderName('date'));
        } else {
            $dateRFC1123 = $request->getHeader('Date');
            if (!$dateRFC1123) {
                return $defaultError;
            }

            //Esta excepção só tem a ver com o test suite da aws. Os testes deles usam uma data inválida... o que provoca falhas na conversão dos formatos
            if ($dateRFC1123 == 'Mon, 09 Sep 2011 23:36:00 GMT') {
                $dateRFC1123 = 'Fri, 09 Sep 2011 23:36:00 GMT';
            }

            $dt = \DateTime::createFromFormat(DateFormat::RFC1123, $dateRFC1123);
            $longDate = $dt->format(DateFormat::ISO8601);
        }

        if (is_array($longDate)) {
            $longDate = $longDate[0];
        }

        if (!$longDate) {
            return $defaultError;
        }
        return $longDate;
    }

    public function getHeaderNamePrefix()
    {
        return $this->config['headersPrefix'];
    }

    public function setHeaderNamePrefix($headerNamePrefix)
    {
        $headerNamePrefix = strtolower($headerNamePrefix);
        if (substr($headerNamePrefix, 0, 2) == 'x-') {
            $this->config['headersPrefix'] = $headerNamePrefix;
        } else {
            $this->config['headersPrefix'] = $this->defaultConfig['headersPrefix'];
        }
        return $this;
    }

    public function startsWithHeaderNamePrefix($key)
    {
        $prefix = $this->getHeaderNamePrefix();
        return (substr($key, 0, strlen($prefix)) === $prefix);
    }

    public function getHeaderName($header)
    {
        return $this->getHeaderNamePrefix().$header;
    }

    public function getSignatureTag()
    {
        return $this->config['version'].'-'.$this->config['hashMethod'].'-'.$this->config['hashAlgo'];
    }

    public function setSignatureTag($signatureTag)
    {
        return $this;
    }

    public function getRequestTag()
    {
        return strtolower($this->config['version'].'_request');
    }

    public function setRequestTag($requestTag)
    {
        return $this;
    }

    public function getSecretKeyPrefix()
    {
        return $this->config['version'];
    }

    public function setSecretKeyPrefix($secretKeyPrefix)
    {
        return $this;
    }

    public function isOverrideDateHeaders()
    {
        return $this->overrideDateHeaders;
    }

    public function setOverrideDateHeaders($overrideDateHeaders)
    {
        $this->overrideDateHeaders = $overrideDateHeaders;
    }

    public function getExtraSignableHeaders()
    {
        return $this->extraSignableHeaders;
    }

    public function setExtraSignableHeaders($extraSignableHeaders)
    {
        $this->extraSignableHeaders = array();
        if (is_string($extraSignableHeaders)) {
            $extraSignableHeaders = array($extraSignableHeaders);
        }
        if (is_array($extraSignableHeaders)) {
            $this->extraSignableHeaders = $extraSignableHeaders;
        }
    }

    public function SHA()
    {
        return $this->getHashAlgo();
    }

    public function setHashAlgo($hashAlgo)
    {
        $hashAlgo = strtoupper($hashAlgo);
        if (!$this->isValidHashAlgo($hashAlgo)) {
            $hashAlgo = $this->defaultConfig['hashAlgo'];
        }
        $this->config['hashAlgo'] = $hashAlgo;
        return $this;
    }

    public function getHashAlgo()
    {
        return strtolower($this->config['hashAlgo']);
    }

    public static function isValidHashAlgo($hashAlgo)
    {
        return (in_array($hashAlgo, self::$validHashAlgos));
    }

    public function getLastSigningContext()
    {
        return $this->_lastSigningContext;
    }

    protected function getTimestamp()
    {
        return time();
    }
}
