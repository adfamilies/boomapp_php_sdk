<?php

namespace BoomApp\SDK;

use BoomApp\SDK\Exceptions\InternalErrorException;
use BoomApp\SDK\Exceptions\InvalidCredentialsException;
use BoomApp\SDK\Exceptions\NoMatchException;
use BoomApp\SDK\Match;
use BoomApp\SDK\Signature\Credentials;
use BoomApp\SDK\Signature\SignatureV1;
use BoomApp\SDK\Utils\DateFormat;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class BoomAppSDK
{
    protected $apiKey;
    protected $apiSecret;

    public $apiBaseUrl = 'https://api-v2.boomapp.co';
    protected $apiVersion = '/v2.0';
    protected $apiMethod = '/matching';
    protected $method = 'POST';
    protected $sdkPlatform = 'PHP';
    protected $mediaParamName = 'matchFile';

    public function __construct($apiKey, $apiSecret)
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }

    public function matching(\SplFileInfo $file)
    {
        $path = $this->apiVersion . $this->apiMethod;
        $url = $this->apiBaseUrl . $path;
        $queryParams = "";

        //$request = new GuzzleRequest($this->method, $path, $queryParams, $options);

        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());
        $stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            $signedHeaders = $this->signRequestWithSignatureV1($request, $this->apiKey, $this->apiSecret);
            // $signedHeaders = array_merge($signedHeaders, [ 'Content-type'          => 'multipart/form-data' ]);

            return new Request(
                $request->getMethod(),
                $request->getUri(),
                $signedHeaders,
                $request->getBody()
            );
        }));

        $httpClient = new Client(['handler' => $stack, 'connect_timeout' => 30, ]);
        $options = [
            'headers' => [
                'x-boom-date'           => gmdate(DateFormat::ISO8601, time()),
                'x-boom-sdk-platform'   => $this->sdkPlatform,
                'x-boom-content-sha256' => 'c633444c758c27f2518c5e27096ffb356790513d6b57792a71ad6a78a4a42c04'
            ],
            'multipart' => [
                [
                    'name' => $this->mediaParamName,
                    'contents' => fopen($file->getPathname(), 'r'),
                ]
            ],
        ];

        try {
            $response = $httpClient->request($this->method, $url . $queryParams, $options);
        } catch (RequestException $e) {
            $this->handleRequestException($e);
        } catch (\Exception $e) {
            throw new InternalErrorException($e->getMessage());
        }

        $this->handleResponseException($response);

        $header = 'HTTP/1.1 ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase();
        $headers = $response->getHeaders();
        array_unshift($headers, $header);
        try {
            $body = json_decode($response->getBody());
        } catch (\Exception $e) {
            throw new InternalErrorException($response->getBody());
        }

        // var_dump("Headers => " . var_export($headers, true) . "\n");
        // var_dump("Body => " . var_export($body, true) . "\n");

        if ($body->status=="OK") {
            $match = new Match(json_encode($body->payload));
        } else {
            throw new InternalErrorException(json_encode($body));
        }

        return $match;
    }

    protected function signRequestWithSignatureV1(Request $request, $key, $secret)
    {
        $signature = new SignatureV1('boomapp', 'europe');
        $signature->setConfig('BA1', 'HMAC', 'SHA256', 'x-boom-');
        $signature->setOverrideDateHeaders(false);
        $credentials = new Credentials($key,  $secret);

        return $signature->signRequest($request, $credentials);

        // return $signature->getLastSigningContext();
    }

    protected function handleRequestException(RequestException $e)
    {
        if ($e->hasResponse()) {
            if ($e->getResponse()->getStatusCode()==403) {
                throw new InvalidCredentialsException($e->getResponse()->getReasonPhrase());
            }
        } else {
            throw new InternalErrorException($e->getResponse()->getReasonPhrase());
        }
    }

    protected function handleResponseException($response)
    {
        $body = json_decode($response->getBody());

        if ($response->getStatusCode()!=200) {
            if ($response->getStatusCode()==400) {
                if ($body->error->reason=="matchUnsuccessful") {
                    throw new NoMatchException($body->error->message);
                }

                if ($body->error->reason=="invalidApiId") {
                    throw new InvalidCredentialsException($body->error->message);
                }
            }
            if ($response->getStatusCode()==403) {
                if ($body->error->reason=="forbidden") {
                    throw new InvalidCredentialsException($body->error->message);
                }
            }

            throw new InternalErrorException($response->getReasonPhrase());
        }
    }
}
