<?php

namespace BoomApp\SDK;

class Match
{

    protected $payload;
    protected $rawPayload;
    
    public function __construct($payload)
    {
        $this->rawPayload = $payload;
        $this->payload = json_decode($payload);
    }

    public function getStartingSecond()
    {
        return (int) $this->payload->starting_second;
    }

    public function getEndingSecond()
    {
        return (int) $this->payload->ending_second;
    }

    public function getMediaId()
    {
        return (int) $this->getMedia()->id;
    }

    public function getMediaName()
    {
        return $this->getMedia()->name;
    }

    public function getMediaTotalBooms()
    {
        return (int) $this->getMedia()->total_booms;
    }

    public function getMedia()
    {
        return $this->payload->media;
    }

    public function getResource()
    {
        return $this->payload->resource;
    }

    public function getRawData()
    {
        return $this->rawPayload;
    }
}
