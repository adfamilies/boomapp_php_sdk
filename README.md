# BoomApp PHP SDK

### Installation
In order to install the php sdk you have to update your ``composer.json`` file with the following code:

```
#!json
{
    "require": {
        "boomapp/php-sdk": "^1.1.1"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/adfamilies/boomapp_php_sdk.git"
        }
    ]
}
```

After doing that you all set to use our SDK.

### How to use

To use the sdk simply follow these 3 steps:

## (1st) GET SDK Tokens

When you have access to our technology you will be able to login into your developers portal where you will be able to find your PHP SDK token keys.

## (2nd) Initialize SDK

```
#!php

$sdk = new BoomApp\SDK\BoomAppSDK("{TOKEN_KEY}", "{TOKEN_SECRET}");
```

Note: Pass your tokens into the constructor.

## (3rd) Start Booming/Matching

After you have constructed the SDK object you can start matching and for that it is required to pass a ``SplFileInfo`` object, so lets fetch it:

```
#!php

$file = new \SplFileInfo("segment.mp4");
```

When we have the file we can start the match:

```
#!php

$match = $sdk->matching($file);
```

Now if everything goes smooth the method will return a ``BoomApp\SDK\Match`` object.

If something went wrong an exception will be thrown which can be:

- ``BoomApp\SDK\Exceptions\NoMatchException``
If the video you are sending did not match any of your detectable medias.

- ``BoomApp\SDK\Exceptions\InvalidCredentialsException``
If the app credentials specific for the PHP SDK you provided are incorrect.

- ``BoomApp\SDK\Exceptions\InternalErrorException``
If there was an internal error with our PHP SDK (Probably our bad! :X)

### Documentation

## BoomApp\SDK\BoomAppSDK

-- ``matching($file)``

Recieves a ``SplFileInfo`` object and proceeds with the matching process, either returns a ``BoomApp\SDK\Match`` object or throws an exception.

## BoomApp\SDK\Match

-- ``getStartingSecond()``

Get the segment matched starting second, returns an (int). 

-- ``getEndingSecond()``

Get the segment matched ending second , returns an (int).

-- ``getMediaId()``

Get the matched media id, returns an (int).

-- ``getMediaName()``

Get the matched media name, returns a (string).

-- ``getMediaTotalBooms()``

Get the matched media total booms (matches), returns an (int).

### Full Example

To see the full source code of this example go to [boomapp_sdk_examples](https://bitbucket.org/adfamilies/boomapp_sdk_examples/) repository.

```
#!php

<?php 

require('vendor/autoload.php');

$key = "";
$secret = "";

$BoomAppSDK = new BoomApp\SDK\BoomAppSDK($key, $secret);

try {
    $file = new \SplFileInfo("segment.mp4");
    $match = $BoomAppSDK->matching($file);
    // Get the segment matched starting second 
    $match->getStartingSecond();
    // Get the segment matched ending second 
    $match->getEndingSecond();
    // Get the matched media id 
    $match->getMediaId();
    // Get the matched media name
    $match->getMediaName();
    // Get the matched media total booms (matches)
    $match->getMediaTotalBooms();
    die("Match!!!\n");
} catch (BoomApp\SDK\Exceptions\NoMatchException $e) {
    die("There was no match...\n");
} catch (BoomApp\SDK\Exceptions\InvalidCredentialsException $e) {
    die("Invalid Credentials error...\n");
} catch (BoomApp\SDK\Exceptions\InternalErrorException $e) {
    die("SDK internal error...\n");
} catch (\Exception $e) {
    var_dump($e->getMessage());
    die("PHP error...\n");
}
```